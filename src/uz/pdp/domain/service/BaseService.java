package uz.pdp.domain.service;

import uz.pdp.domain.DTO.BaseResponse;

import java.util.UUID;

public interface BaseService<CD,RD> {
    BaseResponse add(CD t);
    int remove(RD t);
    CD getById(UUID uuid);
    int remove(UUID uuid);
}
