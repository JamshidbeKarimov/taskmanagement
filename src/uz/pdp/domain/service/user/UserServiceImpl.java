package uz.pdp.domain.service.user;

import uz.pdp.domain.DTO.BaseResponse;
import uz.pdp.domain.DTO.SignUpDTO;
import uz.pdp.domain.model.User;
import uz.pdp.domain.repository.user.UserRepositoryImpl;
import uz.pdp.domain.util.Util;

import java.util.Objects;
import java.util.UUID;

import static uz.pdp.domain.util.Util.userrepository;

public class UserServiceImpl  implements UserService{

    private final UserRepositoryImpl userRepository = UserRepositoryImpl.getInstance();
    @Override
    public BaseResponse add(SignUpDTO userDTO) {
        if (doesPhoneNumberExists(userDTO.phoneNumber())){
            return new BaseResponse("Phone number already exists", 400);
        }
        User user = new User.UserBuilder()
                .setName(userDTO.name())
                .setPhoneNum(userDTO.phoneNumber())
                .setPassword(userDTO.password()).build();
        userRepository.save(user);
         return new BaseResponse("Success",200);
    }

    private boolean doesPhoneNumberExists(String phoneNumber) {
        return true;
    }

    @Override
    public int remove(User t) {
        Util.userrepository.remove(t);
        return 1;
    }

    @Override
    public SignUpDTO getById(UUID uuid) {
        return null;
    }

    @Override
    public int remove(UUID uuid) {
        for (User user : Util.userrepository.getAll()) {
            if (user.getSenderId().equals(uuid)){
                Util.userrepository.remove(user);
                return 1;
            }
        }
        return -1;
    }
    }



