package uz.pdp.domain.service.user;

import uz.pdp.domain.DTO.SignUpDTO;
import uz.pdp.domain.model.User;
import uz.pdp.domain.service.BaseService;

public interface UserService extends BaseService<SignUpDTO,User> {
}
