package uz.pdp.domain.service.tesk;

import uz.pdp.domain.DTO.SignUpDTO;
import uz.pdp.domain.model.Task;
import uz.pdp.domain.model.User;
import uz.pdp.domain.service.BaseService;

public interface TaskService  extends BaseService<Task, SignUpDTO> {

}
