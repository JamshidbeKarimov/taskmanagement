package uz.pdp.domain.service.tesk;

import uz.pdp.domain.DTO.BaseResponse;
import uz.pdp.domain.DTO.SignUpDTO;
import uz.pdp.domain.model.Task;
import uz.pdp.domain.model.User;
import uz.pdp.domain.util.Util;

import java.util.UUID;

public class TaskSrviceImpl implements TaskService{


    @Override
    public BaseResponse add(Task t) {
        return null;
    }

    @Override
    public int remove(SignUpDTO t) {
        return 0;
    }

    @Override
    public Task getById(UUID uuid) {
        return Util.taskrepository.getById(uuid);
    }

    @Override
    public int remove(UUID uuid) {
        for (Task task : Util.taskrepository.getAll()) {
            if (task.getSenderId().equals(uuid)) {
                Util.taskrepository.remove(uuid);
                return 1;
            }
        }
        return -1;
    }
    }

