package uz.pdp.domain.repository;

import java.util.ArrayList;
import java.util.UUID;

public interface BaseRepository<T> {
    int save(T t);
    T getById(UUID id);
    ArrayList<T> getAll();
    void remove(UUID uuid);
    void remove(T t);
}
