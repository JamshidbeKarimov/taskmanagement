package uz.pdp.domain.repository.user;

import uz.pdp.domain.model.User;
import uz.pdp.domain.repository.BaseRepository;

public interface UserRepository extends BaseRepository<User> {

}
