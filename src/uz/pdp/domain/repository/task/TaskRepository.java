package uz.pdp.domain.repository.task;

import uz.pdp.domain.model.Task;
import uz.pdp.domain.repository.BaseRepository;

public interface TaskRepository extends BaseRepository<Task> {
}
