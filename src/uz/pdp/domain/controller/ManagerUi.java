package uz.pdp.domain.controller;

import uz.pdp.domain.model.User;

import static uz.pdp.domain.util.Util.SCANNER_STR;

public class ManagerUi {
    public void managerMenyu(User user){
        int a = 0;
        while (true){
            System.out.println("""
                    1.Create task
                    2.Task assign to anyone
                    3.All workers
                    4.worker's tasks
                    5.Change role of user
                    0.Back
                    """);
            
            a = SCANNER_STR.nextInt();
            
            switch (a){
                case 1 -> createTask(user);
                case 2 -> assign(user);
                case 3 -> allWorkers(user);
                case 4 -> workerTasks(user);
                case 5 -> changeRole(user);
                case 0 -> {
                    return;
                }
                default -> System.out.println(" Wrong input ");
            }
        }
    }

    private void createTask(User user) {
    }

    private void assign(User user) {
    }

    private void allWorkers(User user) {
    }

    private void workerTasks(User user) {
    }

    private void changeRole(User user) {
    }
}
