package uz.pdp.domain.DTO;

public class BaseResponse<T> {
    private String massage;
    private T data;
    private int status;

    public BaseResponse(String massage, T data, int status) {
        this.massage = massage;
        this.data = data;
        this.status = status;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public BaseResponse(String massage, int status) {
        this.massage = massage;
    }

    public String getMassage() {
        return massage;
    }

    public void setMassage(String massage) {
        this.massage = massage;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
