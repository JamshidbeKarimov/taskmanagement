package uz.pdp.domain.model;

public enum UserRole {
    MANAGER,
    BACKEND_LEAD,
    FRONTEND_LEAD,
    BACKEND_DEV,
    FRONTEND_DEV,
    TESTER,
    USER;
}
