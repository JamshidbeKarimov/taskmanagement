package uz.pdp.domain.model;

import java.util.UUID;

public class Task extends BaseModel{
    private String text;
    private TaskStatus status;
    private UUID senderId;
    private UUID reseiverId;


    public Task(TaskBuilder taskBuilder, UUID reseiverId) {
        this.status = taskBuilder.status;
        this.text = taskBuilder.text;
        this.senderId = taskBuilder.uuid;
        this.reseiverId = reseiverId;
    }

    public static class TaskBuilder{
        private String text;
        private TaskStatus status;
        private UUID uuid;
        private UUID reseiverId;

        public TaskBuilder setReseiverId(UUID reseiverId) {
            this.reseiverId = reseiverId;
            return this;
        }

        public TaskBuilder setText(String text) {
            this.text = text;
          return this;
        }

        public TaskBuilder setStatus(TaskStatus status) {
            this.status = status;
          return this;
        }

        public TaskBuilder setUuid(UUID uuid) {
            this.uuid = uuid;
          return this;
        }

        public Task build(){
            return new Task(this, reseiverId);
        }

    }

    public UUID getReseiverId() {
        return reseiverId;
    }

    public void setReseiverId(UUID reseiverId) {
        this.reseiverId = reseiverId;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public TaskStatus getStatus() {
        return status;
    }

    public void setStatus(TaskStatus status) {
        this.status = status;
    }

    @Override
    public UUID getSenderId() {
        return senderId;
    }

    @Override
    public void setSenderId(UUID senderId) {
        this.senderId = senderId;
    }
}
