package uz.pdp.domain.model;

import java.util.UUID;

public  abstract  class BaseModel {
    {
        this.uuid = UUID.randomUUID();
    }
    protected UUID uuid;

    public UUID getSenderId() {
        return uuid;
    }

    public void setSenderId(UUID senderId) {
        this.uuid = senderId;
    }
}
