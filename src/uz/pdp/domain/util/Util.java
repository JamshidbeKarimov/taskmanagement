package uz.pdp.domain.util;

import uz.pdp.domain.repository.task.TaskRepository;
import uz.pdp.domain.repository.task.TaskRepositoryImpl;
import uz.pdp.domain.repository.user.UserRepository;
import uz.pdp.domain.repository.user.UserRepositoryImpl;
import uz.pdp.domain.service.tesk.TaskService;
import uz.pdp.domain.service.tesk.TaskSrviceImpl;
import uz.pdp.domain.service.user.UserService;
import uz.pdp.domain.service.user.UserServiceImpl;

import java.util.Scanner;

public interface Util {

    Scanner SCANNER_NUM =  new Scanner(System.in);
    Scanner SCANNER_STR = new Scanner(System.in);
    UserService userservice = new UserServiceImpl();
    TaskService taskservice = new TaskSrviceImpl();

    UserRepository userrepository = new UserRepositoryImpl();
    TaskRepository taskrepository = new TaskRepositoryImpl();

}
